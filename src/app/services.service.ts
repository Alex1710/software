import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ServicesService {

  constructor(private http:HttpClient) { }

  //Esto es el servicio para que se conecte con la Api
  //se envia el formulario y se necesita la url de la API
  //url: http://34343uygshd.com/almacenarDatos
  enviarDatos(formulario:any):Observable<any>{
    return this.http.post('url', formulario);
  }

}
