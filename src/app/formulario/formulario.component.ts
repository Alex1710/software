import { Component, OnInit } from '@angular/core';
import { FormControl, FormBuilder, FormGroup } from '@angular/forms';
import { ServicesService } from '../services.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.scss']
})
export class FormularioComponent implements OnInit {
  formulario:FormGroup;
  constructor(private formBuilder:FormBuilder,
              private enviarDatosBackend: ServicesService,
              private router:Router) { 
    this.buildForm();
  }
  buildForm(){
    this.formulario = this.formBuilder.group({
      name:[''],
      phone:[''],
      nit: [''],
      hrIni: ['']
    })
  }
  ngOnInit(): void {
  }
  enviarDatos(){
    console.log(this.formulario.value);
    //this.enviarDatosBackend.enviarDatos(this.formulario.value).subscribe(
    //result =>{
      //console.log(result);
      this.router.navigate(['/formulario2']);//url del otro formulario(componente)
    //},
    //error=>{
      //console.log(error);
    }
    //);
  //}
}
